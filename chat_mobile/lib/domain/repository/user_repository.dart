import 'package:chat_models/chat_models.dart';

abstract class UserRepository {
  Future<User> getUser();
  Future<void> setUser(User user);
  Future<void> logout();
  Future<String> getToken();
  Future<void> setToken(String token);
}