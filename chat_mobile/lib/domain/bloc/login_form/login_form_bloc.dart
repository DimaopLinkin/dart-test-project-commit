import 'package:bloc/bloc.dart';
import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/domain/bloc/login_form/password_field_bloc.dart';
import 'package:chat_mobile/domain/bloc/login_form/text_field_bloc.dart';
import 'package:chat_mobile/domain/repository/user_repository.dart';
import 'package:chat_mobile/internal/api_client.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/widgets.dart';
import 'package:chat_mobile/data/globals.dart' as globals;

class LoginFormBloc extends Bloc<LoginFormEvent, LoginFormState> {
  final UserRepository _userDataRepository;
  final TextFieldBloc nameBloc;
  final PasswordFieldBloc passwordBloc;

  LoginFormBloc(
      this.nameBloc, this.passwordBloc, this._userDataRepository);

  @override
  LoginFormState get initialState => InitLoginFormState();

  @override
  Future<void> close() {
    nameBloc.close();
    passwordBloc.close();
    return super.close();
  }

  @override
  Stream<LoginFormState> mapEventToState(LoginFormEvent event) async* {
    if (event is TryLoginFormEvent) {
      yield await _mapLoginToState(event);
    }
    if (event is TrySignUpFormEvent) {
      yield await _mapSignUpToState(event);
    }
    if (event is UnFocusLoginFormEvent) {
      if (nameBloc.focusNode.hasFocus) {
        nameBloc.focusNode.unfocus();
      }
      if (passwordBloc.focusNode.hasFocus) {
        passwordBloc.focusNode.unfocus();
      }
    }
  }

  Future<LoginFormState> _mapLoginToState(TryLoginFormEvent event) async {
    try {
      UsersClient usersClient = UsersClient(MobileApiClient());
      var user =
      await usersClient.login(nameBloc.value, passwordBloc.value);
      await _userDataRepository.setUser(user);
      await _userDataRepository.setToken(globals.authToken);
      globals.currentUser = user;
      return SuccessLoginFormState();
    } on Exception catch (e) {
      print('Login failed');
      print(e);
      return FailedLoginFormState();
    }
  }

  Future<LoginFormState> _mapSignUpToState(TrySignUpFormEvent event) async {
    UsersClient usersClient = UsersClient(MobileApiClient());
    usersClient
        .create(User(name: nameBloc.value, password: passwordBloc.value))
        .then((createdUser) {
      return SuccessSignUpFormState();
    }).catchError((signUpError) {
      print('Sign up failed');
      print(signUpError);
      return FailedSignUpFormState();
    });
    return SuccessSignUpFormState();
  }
}
// TODO: Придумать как внедрить в BLoC
// Попытка авторизации с сохраненными данными
// _startupLogin(BuildContext context) async {
//   SharedPreferences prefs = await SharedPreferences.getInstance();
//   if (prefs.getString('authToken') != null) {
//     globals.authToken = prefs.getString('authToken');
//     try {
//       UsersClient usersClient = UsersClient(MobileApiClient());
//       var user = await usersClient.login(
//           prefs.getString('login'), prefs.getString('password'));
//       globals.currentUser = user;
//       Navigator.pushNamed(context, '/chat_list').then((_) {
//         globals.currentUser = null;
//         globals.authToken = null;
//         prefs.setString('authToken', null);
//         prefs.setString('login', null);
//         prefs.setString('password', null);
//       });
//     } on Exception catch (e) {
//       final snackBar = SnackBar(content: Text('Login failed'));
//       Scaffold.of(context).showSnackBar(snackBar);
//       print('Login failed');
//       print(e);
//     }
//   }
// }

@immutable
abstract class LoginFormEvent {}

class TryLoginFormEvent extends LoginFormEvent {}
class TrySignUpFormEvent extends LoginFormEvent {}
class UnFocusLoginFormEvent extends LoginFormEvent {}

@immutable
abstract class LoginFormState {}

class InitLoginFormState extends LoginFormState {}
class SuccessLoginFormState extends LoginFormState {}
class FailedLoginFormState extends LoginFormState {}
class SuccessSignUpFormState extends LoginFormState {}
class FailedSignUpFormState extends LoginFormState {}
