import 'package:chat_mobile/domain/chat_component.dart';
import 'package:chat_mobile/presentation/chat_list.dart';
import 'package:chat_mobile/presentation/user_details_ui.dart';
import 'file:///G:/dart-test-project-commit/dart-test-project-commit/chat_mobile/lib/presentation/login/login.dart';

import 'package:flutter/material.dart';

import 'package:chat_mobile/data/globals.dart' as globals;

class SimpleChatApp extends StatefulWidget {
  final ChatComponent _chatComponent = ChatComponent(globals.webSocketAddress);

  @override
  _SimpleChatAppState createState() => _SimpleChatAppState();
}

class _SimpleChatAppState extends State<SimpleChatApp> {
  @override
  void initState() {
    super.initState();
    widget._chatComponent.connect();
  }

  @override
  void dispose() {
    widget._chatComponent.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ChatComponentWidget(
      widget._chatComponent,
      MaterialApp(
        title: 'Simple Chat',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        // home: LoginPage(),
        initialRoute: '/',
        routes: {
          '/': (context) => LoginPage(),
          '/chat_list': (context) => ChatListPage(
            title: "${globals.currentUser.name}",
            chatComponent: widget._chatComponent,
          ),
          '/user_details': (context) => DetailsPage(),
        },
      ),
    );
  }
}