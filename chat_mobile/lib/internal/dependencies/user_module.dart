import 'package:chat_mobile/domain/bloc/login_form/login_form_bloc.dart';
import 'package:chat_mobile/domain/bloc/login_form/password_field_bloc.dart';
import 'package:chat_mobile/domain/bloc/login_form/text_field_bloc.dart';
import 'package:chat_mobile/domain/bloc/logout_bloc.dart';
import 'package:chat_mobile/internal/dependencies/user_repository_module.dart';

class UserModule {
  static LogoutBloc logoutBloc() {
    return LogoutBloc(UserRepositoryModule.userRepository);
  }

  static TextFieldBloc textFieldBloc() {
    return TextFieldBloc();
  }

  static PasswordFieldBloc passwordFieldBloc() {
    return PasswordFieldBloc();
  }

  static LoginFormBloc loginFormBloc() {
    return LoginFormBloc(textFieldBloc(), passwordFieldBloc(),
        UserRepositoryModule.userRepository);
  }
}
