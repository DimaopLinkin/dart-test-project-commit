

import 'package:chat_mobile/data/repository/user_data_repository.dart';
import 'package:chat_mobile/internal/dependencies/sp_module.dart';

class UserRepositoryModule {
  static final userRepository = UserDataRepository(SpModule.spUtil());
}