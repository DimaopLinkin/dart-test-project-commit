import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/internal/api_client.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';

import '../data/globals.dart' as globals;

class DetailsPage extends StatefulWidget {
  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  _UserData _userData = _UserData(
      globals.currentUser.id,
      globals.currentUser.name,
      globals.currentUser.password,
      globals.currentUser.lastName ?? "Empty",
      globals.currentUser.email ?? "Empty",
      globals.currentUser.phone ?? "Empty");
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();

  String _validateEmail(String value) {
    if (!globals.emailValidatorRegExp.hasMatch(value) && value.isNotEmpty) {
      // check email rules here
      return 'Enter valid email, example@email.com';
    }
    return null;
  }

  String _validateName(String value) {
    if (value.length < 2 && value.isNotEmpty) {
      // check login rules here
      return 'The Last Name must be at least 2 characters.';
    }
    return null;
  }

  String _validatePhone(String value) {
    if (!globals.phoneValidatorRegExp.hasMatch(value) && value.isNotEmpty) {
      // check email rules here
      return 'Enter valid number, example 79995552233';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Details ${_userData.name}"),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Form(
        key: this._formKey,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextFormField(
                  controller: _nameController,
                  onSaved: (newValue) => _userData.lastName = newValue,
                  validator: this._validateName,
                  decoration: InputDecoration(
                    hintText: "New Last Name",
                    labelText:
                        "Last Name ${globals.currentUser.lastName ?? 'empty'}",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextFormField(
                  controller: _emailController,
                  keyboardType: TextInputType.emailAddress,
                  onSaved: (newValue) => _userData.email = newValue,
                  validator: this._validateEmail,
                  decoration: InputDecoration(
                    hintText: "New Email",
                    labelText: "email ${globals.currentUser.email ?? 'empty'}",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextFormField(
                  controller: _phoneController,
                  keyboardType: TextInputType.phone,
                  onSaved: (newValue) => _userData.phone = newValue,
                  validator: this._validatePhone,
                  decoration: InputDecoration(
                    hintText: "New phone number",
                    labelText: "Phone ${globals.currentUser.phone ?? 'empty'}",
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                child: RaisedButton(
                  child: Text("Update"),
                  onPressed: () {
                    _update(context);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _update(BuildContext context) {
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save();

      try {
        UsersClient usersClient = UsersClient(MobileApiClient());
        usersClient
            .update(User(
          id: globals.currentUser.id,
          name: _userData.name,
          lastName: _userData.lastName == ''
              ? globals.currentUser.lastName
              : _userData.lastName,
          password: _userData.password,
          email: _userData.email == ''
              ? globals.currentUser.email
              : _userData.email,
          phone: _userData.phone == ''
              ? globals.currentUser.phone
              : _userData.phone,
        ))
            .then((updatedUser) {
          globals.currentUser = updatedUser;
          _clearUi();
          setState(() {});
        });
      } on Exception catch (e) {
        print('Update failed');
        print(e);
      }
    }
  }

  void _clearUi() {
    _nameController.clear();
    _emailController.clear();
    _phoneController.clear();
  }
}

class _UserData {
  UserId id;
  String name;
  String password;
  String lastName;
  String email;
  String phone;

  @override
  _UserData(UserId id, String name, String password, String lastName,
      String email, String phone) {
    this.id = id;
    this.name = name;
    this.password = password;
    this.lastName = lastName;
    this.email = email;
    this.phone = phone;
  }
}
