import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';

import '../internal/api_client.dart';
import '../domain/chat_component.dart';
import 'chat_content.dart';
import '../data/globals.dart' as globals;

class UserListPage extends StatefulWidget {
  UserListPage({Key key}) : super(key: key);

  @override
  _UserListPageState createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  var _checkableUsers = <_CheckableUser>[];

  @override
  void initState() {
    super.initState();
    refreshUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Expanded(
          child: RefreshIndicator(
            onRefresh: () async {
              refreshUsers();
            },
            child: ListView.builder(
              itemCount: _checkableUsers.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  child: _buildListTile(_checkableUsers[index]),
                  color: _checkableUsers[index].isChecked
                      ? Colors.blue[200]
                      : Colors.transparent,
                );
              },
            ),
          ),
        ),
        buildButton(_checkableUsers),
      ],
    );
  }

  Padding buildButton(List _checkableUsers) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: isCheckedSomeone(_checkableUsers)
          ? FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                createChat();
              },
            )
          : Container(),
    );
  }

  ListTile _buildListTile(_CheckableUser checkableUser) {
    return ListTile(
      onTap: () {
        setState(() {
          checkableUser.isChecked = !checkableUser.isChecked;
        });
      },
      title: Row(
        children: [
          CircleAvatar(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(checkableUser.user.name?.substring(0, 1)?.toUpperCase() ??
                    "#"),
                Text(checkableUser.user.lastName
                        ?.substring(0, 1)
                        ?.toUpperCase() ??
                    "#"),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(checkableUser.user.email ?? "email"),
                Text(checkableUser.user.name ?? "noname"),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void refreshUsers() async {
    try {
      UsersClient _usersClient = UsersClient(MobileApiClient());
      List<User> found = await _usersClient.read({});
      found.removeWhere((user) => user.id == globals.currentUser.id);
      setState(() {
        _checkableUsers = found.map((foundUser) {
          return _CheckableUser(user: foundUser);
        }).toList();
      });
    } on Exception catch (e) {
      print('Failed to get list of users');
      print(e);
    }
  }

  bool isCheckedSomeone(List _checkableUsers) {
    for (int index = 0; index < _checkableUsers.length; index++) {
      if (_checkableUsers[index].isChecked) return true;
    }
    return false;
  }

  void createChat() async {
    var _checkedCounterparts = _checkableUsers
        .where((checkableUser) => checkableUser.isChecked == true)
        .map((checkableUser) => checkableUser.user)
        .toList();
    if (_checkedCounterparts.isNotEmpty) {
      try {
        ChatsClient chatsClient = ChatsClient(MobileApiClient());
        Chat createdChat = await chatsClient.create(
            Chat(members: _checkedCounterparts..add(globals.currentUser)));
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => ChatContentPage(
              chat: createdChat,
              chatComponent: ChatComponentWidget.of(context).chatComponent,
            ),
          ),
        );
      } on Exception catch (e) {
        print('Chat creation failed');
        print(e);
      }
    }
  }
}

class _CheckableUser {
  final User user;
  bool isChecked;

  _CheckableUser({
    this.user,
    this.isChecked = false,
  });
}
