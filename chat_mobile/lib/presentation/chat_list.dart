import 'dart:async';
import 'dart:collection';

import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_mobile/presentation/users_list.dart';
import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import '../internal/api_client.dart';
import '../domain/chat_component.dart';
import 'chat_content.dart';
import 'common_ui.dart';

class ChatListPage extends StatefulWidget {
  ChatListPage({Key key, this.title, @required this.chatComponent})
      : super(key: key);
  final String title;
  final ChatComponent chatComponent;

  @override
  _ChatListPageState createState() => _ChatListPageState();
}

class _ChatListPageState extends State<ChatListPage> {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  GlobalKey<RefreshIndicatorState> refreshKey;
  var _chats = <Chat>[];
  Set<ChatId> _unreadChats = HashSet<ChatId>();
  StreamSubscription<Set<ChatId>> _unreadMessagesSubscription;

  @override
  void initState() {
    super.initState();
    refreshKey = GlobalKey<RefreshIndicatorState>();
    refreshChats();
    _unreadMessagesSubscription = widget.chatComponent
        .subscribeUnreadMessagesNotification((unreadChatIds) {
      unreadChatIds.forEach((element) {
        if (!_unreadChats.contains(element)) {
          showNotification(element);
        }
      });
      setState(() {
        _unreadChats.clear();
        _unreadChats.addAll(unreadChatIds);
        refreshChats();
      });
    });

    // Инициализация уведомлений
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var android = AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOS = IOSInitializationSettings();
    var initSettings = InitializationSettings(android, iOS);
    flutterLocalNotificationsPlugin.initialize(initSettings,
        onSelectNotification: selectNotification);
  }

  // Функция вызова уведомления. Передаётся chatId
  showNotification(element) async {
    var android = AndroidNotificationDetails(
      'channel id',
      'channelName',
      'CHANNEL DESCRIPTION',
    );
    var iOS = IOSNotificationDetails();
    var platform = NotificationDetails(android, iOS);
    await flutterLocalNotificationsPlugin.show(
        0, 'You have new message', 'Flutter Local Notification', platform,
        payload: element.toString());
  }

  // Функция "onTap" для уведомления. Принимает chatId в виде payload строки.
  // С помощью функции findChat находит необходимый экземпляр chat, и строит
  // MaterialPageRoute
  Future selectNotification(String payload) {
    var chat = findChat(ChatId(payload), _chats);
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return ChatContentPage(
            chat: chat,
            chatComponent: ChatComponentWidget.of(context).chatComponent,
          );
        },
      ),
    );
  }

  // Поиск экземпляра chat по chatId
  Chat findChat(ChatId id, List<Chat> chats) {
    Chat chat;
    chats.forEach((element) {
      if (element.id == id) chat = element;
    });
    return chat;
  }

  @override
  void dispose() {
    _unreadMessagesSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Iterable<Widget> listTiles =
        _chats.map<Widget>((Chat chatItem) => _buildListTile(chatItem));
    listTiles = ListTile.divideTiles(context: context, tiles: listTiles);
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.account_circle,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/user_details').then((_) {});
            },
          ),
          backgroundColor: Colors.blue,
          title: Text(widget.title),
          // TODO: Прикрутить logoutBloc
          actions: <Widget>[LogoutButton()],
          automaticallyImplyLeading: false,
          bottom: TabBar(
            indicatorColor: Colors.white,
            onTap: (index) {},
            tabs: [
              Tab(
                child: Container(
                  child: Text(
                    'Chat List',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
              Tab(
                child: Container(
                  child: Text(
                    'Users',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        body: Builder(builder: (BuildContext scaffoldContext) {
          return Container(
            child: TabBarView(
              children: [
                RefreshIndicator(
                  key: refreshKey,
                  onRefresh: () async {
                    refreshChats();
                  },
                  child: ListView(
                    children: listTiles.toList(),
                  ),
                ),
                UserListPage(),
              ],
            ),
          );
        }),
      ),
    );
  }

  Widget _buildListTile(Chat chat) {
    return Container(
      child: ListTile(
        leading:
            _unreadChats.contains(chat.id) ? const Icon(Icons.message) : null,
        title: Text(chat.members.map((user) => user.name).join(", ")),
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return ChatContentPage(
                  chat: chat,
                  chatComponent: ChatComponentWidget.of(context).chatComponent,
                );
              },
            ),
          );
        },
      ),
    );
  }

  void refreshChats() async {
    try {
      List<Chat> found = await ChatsClient(MobileApiClient()).read({});
      setState(() {
        _chats = found;
      });
    } on Exception catch (e) {
      print('Failed to get list of chats');
      print(e);
    }
  }
}
