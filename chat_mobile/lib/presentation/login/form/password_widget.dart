import 'package:chat_mobile/domain/bloc/login_form/password_field_bloc.dart';
import 'package:flutter/material.dart';

class PasswordWidget extends StatefulWidget {
  final PasswordFieldBloc passwordBloc;

  PasswordWidget(this.passwordBloc);

  @override
  _PasswordWidgetState createState() => _PasswordWidgetState();
}

class _PasswordWidgetState extends State<PasswordWidget> {
  final TextEditingController _passwordController = TextEditingController();

  String _validatePassword(String value) {
    if (value.length < 2) {
      // check password rules here
      return 'The Password must be at least 2 characters.';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: _passwordController,
      obscureText: true,
      validator: this._validatePassword,
      onChanged: (password) =>
          _onUpdateBlocPassword(password, widget.passwordBloc),
      decoration: InputDecoration(
          hintText: 'Password', labelText: 'Enter your password'),
    );
  }

  void _onUpdateBlocPassword(String password, PasswordFieldBloc passwordBloc) {
    passwordBloc.add(PasswordFieldUpdateEvent(password));
  }
}
