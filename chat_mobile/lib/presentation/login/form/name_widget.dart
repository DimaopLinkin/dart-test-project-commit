import 'package:flutter/material.dart';

import 'package:chat_mobile/domain/bloc/login_form/text_field_bloc.dart';

class NameWidget extends StatefulWidget {
  final TextFieldBloc nameBloc;

  NameWidget(this.nameBloc);

  @override
  _NameWidgetState createState() => _NameWidgetState();
}

class _NameWidgetState extends State<NameWidget> {
  final TextEditingController _loginController = TextEditingController();

  String _validateLogin(String value) {
    if (value.length < 2) {
      // check login rules here
      return 'The Login must be at least 2 characters.';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: _loginController,
      focusNode: widget.nameBloc.focusNode,
      onChanged: (name) => _onUpdateBlocName(name, widget.nameBloc),
      validator: this._validateLogin,
      decoration:
          InputDecoration(hintText: 'Login', labelText: 'Enter your login'),
    );
  }

  void _onUpdateBlocName(String name, TextFieldBloc nameBloc) {
    nameBloc.add(TextFieldUpdateEvent(name));
  }
}
