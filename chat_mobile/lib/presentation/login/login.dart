import 'package:chat_mobile/domain/bloc/login_form/login_form_bloc.dart';
import 'package:chat_mobile/internal/dependencies/user_module.dart';
import 'package:chat_mobile/presentation/login/form/name_widget.dart';
import 'package:chat_mobile/presentation/login/form/password_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final LoginFormBloc _loginFormBloc = UserModule.loginFormBloc();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    _loginFormBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          _loginFormBloc.add(UnFocusLoginFormEvent());
        },
        child: BlocListener<LoginFormBloc, LoginFormState>(
          bloc: _loginFormBloc,
          listener: (context, state) {
            if (state is SuccessLoginFormState) {
              _onLoginSuccess();
            }
            if (state is FailedLoginFormState) {
              _onLoginFailed(context);
            }
            if (state is SuccessSignUpFormState) {
              _onSignUpSuccess(context);
            }
            if (state is FailedSignUpFormState) {
              _onSignUpFailed(context);
            }
          },
          child: BlocBuilder<LoginFormBloc, LoginFormState>(
            bloc: _loginFormBloc,
            builder: (context, loginFormState) {
              return Container(
                padding: EdgeInsets.all(20.0),
                child: Center(
                  child: Form(
                    key: this._formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        NameWidget(_loginFormBloc.nameBloc),
                        PasswordWidget(_loginFormBloc.passwordBloc),
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              RaisedButton(
                                child: Text("Login"),
                                onPressed: () {
                                  _onLogin();
                                },
                              ),
                              FlatButton(
                                child: Text("Sign up"),
                                onPressed: () {
                                  _onSignUp();
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void _onLogin() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _loginFormBloc.add(TryLoginFormEvent());
  }

  void _onLoginSuccess() {
    Navigator.pushNamed(
      context,
      '/chat_list',
    );
  }

  void _onLoginFailed(BuildContext context) {
    final snackBar = SnackBar(content: Text('Login failed'));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  void _onSignUp() {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _showDialog(_loginFormBloc.nameBloc.value).then((resultValue) {
      if (resultValue != null && resultValue is bool && resultValue) {
        _loginFormBloc.add(TrySignUpFormEvent());
      }
    });
  }

  void _onSignUpSuccess(BuildContext context) {
    final snackBar = SnackBar(
        content: Text('User \'${_loginFormBloc.nameBloc.value}\' created'));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  void _onSignUpFailed(BuildContext context) {
    final snackBar = SnackBar(content: Text('Sign up failed'));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  Future<bool> _showDialog(String username) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Text("Do you want to create user ‘$username’ ?"),
          actions: <Widget>[
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
  }
}
