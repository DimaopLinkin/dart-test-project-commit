library chat_mobile.globals;

import 'package:chat_models/chat_models.dart';

const String host = '192.168.0.2';
const String webSocketAddress = 'ws://$host:3333/ws';
const String chatApiAddress = 'http://$host:3333';

final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

final RegExp phoneValidatorRegExp = RegExp(r"^(?:[+0]9)?[0-9]{10,13}$");

String authToken;
User currentUser;
