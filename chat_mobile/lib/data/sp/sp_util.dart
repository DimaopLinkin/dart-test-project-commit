import 'package:shared_preferences/shared_preferences.dart';

class SpUtil {
  static Future<SharedPreferences> get _getSp async =>
      SharedPreferences.getInstance();

  static final _userName = 'user_name';
  static final _userPassword = 'user_password';
  static final _userToken = 'user_token';
  // Не уверен, стоит ли хранить это в SP
  static final _userId = 'user_id';
  static final _userLastName = 'user_last_name';
  static final _userEmail = 'user_email';
  static final _userPhone = 'user_phone';

  Future<String> getName() async {
    final prefs = await _getSp;
    return prefs.getString(_userName) ?? '';
  }

  Future<void> setName(String name) async {
    final prefs = await _getSp;
    await prefs.setString(_userName, name);
  }

  Future<String> getPassword() async {
    final prefs = await _getSp;
    return prefs.getString(_userPassword) ?? '';
  }

  Future<void> setPassword(String password) async {
    final prefs = await _getSp;
    await prefs.setString(_userPassword, password);
  }

  Future<String> getToken() async {
    final prefs = await _getSp;
    return prefs.getString(_userToken) ?? '';
  }

  Future<void> setToken(String token) async {
    final prefs = await _getSp;
    await prefs.setString(_userToken, token);
  }

  Future<String> getId() async {
    final prefs = await _getSp;
    return prefs.getString(_userId) ?? '';
  }

  Future<void> setId(String id) async {
    final prefs = await _getSp;
    await prefs.setString(_userId, id);
  }

  Future<String> getLastName() async {
    final prefs = await _getSp;
    return prefs.getString(_userLastName) ?? '';
  }

  Future<void> setLastName(String lastName) async {
    final prefs = await _getSp;
    await prefs.setString(_userLastName, lastName);
  }

  Future<String> getEmail() async {
    final prefs = await _getSp;
    return prefs.getString(_userEmail) ?? '';
  }

  Future<void> setEmail(String email) async {
    final prefs = await _getSp;
    await prefs.setString(_userEmail, email);
  }

  Future<String> getPhone() async {
    final prefs = await _getSp;
    return prefs.getString(_userPhone) ?? '';
  }

  Future<void> setPhone(String phone) async {
    final prefs = await _getSp;
    await prefs.setString(_userPhone, phone);
  }
}
