import 'package:chat_mobile/data/sp/sp_util.dart';
import 'package:chat_mobile/domain/repository/user_repository.dart';
import 'package:chat_models/chat_models.dart';

class UserDataRepository extends UserRepository {
  SpUtil _spUtil;

  UserDataRepository(this._spUtil);

  @override
  Future<User> getUser() async {
    final id = await _spUtil.getId();
    final name = await _spUtil.getName();
    final password = await _spUtil.getPassword();
    final lastName = await _spUtil.getLastName();
    final email = await _spUtil.getEmail();
    final phone = await _spUtil.getPhone();

    return User(
        id: UserId(id),
        name: name,
        password: password,
        lastName: lastName,
        email: email,
        phone: phone);
  }

  @override
  Future<String> getToken() async {
    final token = await _spUtil.getToken();
    return token;
  }

  @override
  Future<void> setToken(String token) async {
    await _spUtil.setToken(token);
  }

  @override
  Future<void> logout() async {
    await _spUtil.setId('');
    await _spUtil.setName('');
    await _spUtil.setPassword('');
    await _spUtil.setLastName('');
    await _spUtil.setEmail('');
    await _spUtil.setPhone('');
    await _spUtil.setToken('');
  }

  @override
  Future<void> setUser(User user) async {
    await _spUtil.setId(user.id.toString());
    await _spUtil.setName(user.name);
    await _spUtil.setPassword(user.password);
    await _spUtil.setLastName(user.lastName);
    await _spUtil.setEmail(user.email);
    await _spUtil.setPhone(user.phone);
  }
}
